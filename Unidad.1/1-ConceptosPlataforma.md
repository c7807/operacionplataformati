# Conceptos de Plataforma

## Introducción
A modo de introducción y con una definición muy básica, la plataforma se entiende por la infraestructura (equipos de red, servidores, sistema operativo, entre otros) y servicios que se dispone para que un sistema informático pueda funcionar. Esto implica, sin que la lista sea excluyente, la arquitectura de la red, ruteadores, firewall, resolución de DNS, servidores, unidades de disco en red o de almacenamiento, unidades de respaldo, balanceadores de carga, entre otros componentes más ligados al hardware o infraestructura. Además se deben incorporar los componentes lógicos, como puede ser el sistema operativo, controladores, servidor web, motores de base de datos, etc, relacionados propiamente tal a la plataforma.

En la historia de la informática, estos componentes que antiguamente eran máquinas y cables de red que conectaban unas y otras. Hoy se pueden encontrar de forma virtual. Obviamente para que existan de forma virtual, existe una serie de equipos físicos que la soportan, pero estos equipos ya no se encuentran en los centros de datos propios o arrendados por las empresas locales, sino que en la nube<sup>1</sup>.

## A qué se refieren con On Premise
El término _On Premise_ o en local se refiere al tipo de instalación de una solución de software. Esta instalación se lleva a cabo dentro del servidor y la infraestructura (TIC) de la empresa. Es el modelo tradicional de aplicaciones empresariales.

Un ejemplo de un servidor On Premise, puede ser el de un ADDS (Active Directory Domain Server), en el cual se administran las GPO (políticas de grupo) sobre los equipos o personal de la empresa local, este servidor puede estar federado a un equipo en la nube, pero se maneja local por el performance en el momento del inicio del sistema.

## Infraestructura como servicio (IaaS)
La infraestructura es el soporte digital que despliega un sistema informático; son los elementos fundamentales, como servidores y almacenamiento, redes, seguridad y centros de datos. Tradicionalmente, estos aspectos de la informática serían responsabilidad del propio departamento de TI de la organización, alojado y administrado bajo su propio techo.

**IaaS** les da a las empresas la opción de tener un proveedor que proporcione y administre estos elementos de hardware. Este servicio permite a las empresas deshacerse de la responsabilidad de tener que operar su propia infraestructura.

A través de **IaaS**, las empresas pueden acceder al hardware que necesitan en cada momento, a nivel de infraestructura, almacenamiento y capacidad de red a través de Internet, un entorno de TI que se adapte a sus necesidades y se puede ampliar o reducir según sea necesario. Los proveedores de **IaaS** pueden administrar servicios como firewalls, conexiones de red, balanceadores de carga, almacenamiento de datos y administración de identidades. Con estos recursos atendidos, las empresas acceden a esta infraestructura y desarrollarla, instalar sistemas operativos, crear bases de datos y almacenar archivos.

## Plataforma como servicio (PaaS)
Si **IaaS** se encuentra en la parte inferior de la pirámide de computación en la nube, que cubre la infraestructura fundamental en la que se basan todos los demás factores de computación, entonces **PaaS** se encuentra en el medio, siendo la punta de la pirámide el Software como Servicio (**SaaS**).

Con **PaaS**, las empresas pueden crear y desarrollar sus propios servicios en una plataforma existente, utilizando herramientas que pueden obtener de un proveedor externo que se hará cargo de todas las actualizaciones y la administración de esa plataforma. **PaaS** hace que las empresas construyan sus productos y servicios más rápido, más barato y más sencillo, y aumente sus recursos según sea necesario sin un desembolso financiero significativo.

La principal diferencia entre **IaaS** y **PaaS** es que con **PaaS**, el cliente consume el servicio central necesario para las aplicaciones propias, y la administración interna se deja al proveedor de la nube.

## Ejemplos
- **IaaS**: Montar un servidor en la nube, por ejemplo un AWS EC2.
- **PaaS**: AWS EKS, provee la plataforma de Kubernetes para administrar y publicar los docker con servicios propios.
- **SaaS**: GitHub, las personas consumen el software de GitHub y trabajan directamente con él, no tienen un servidor git en la empresa.

## Para pensar
- Con IaaS ahorro problemas de comprar servidores de respaldo, discos, estar preocupado de los RAID, y otros problemas físicos.
- Con PaaS me despreocupo de las actualizaciones del sistema operativo, de los parches de seguridad, escabilidad y similares.
- Con SaaS me despreocupo de la seguridad del sistema y de las actualizaciones del sistema, solamente consumo el software.
- Muchas soluciones en la actualidad mezclan IaaS, PaaS y/o SaaS dependiendo del tipo de solución y de las API de integración. Ejemplo: Puedo desplegar un sitio web propio creado en Pyhton y Angular, cuyo código está en GitLab (SaaS), tal que al hacer un push, ejecute un depliegue continuo (con GitLab CI/CD), creando una imagen docker que se despliega hacia kubernetes (PaaS) y crea un balanceador de carga (IaaS) tal que el sitio tenga alta disponibilidad.

## Para investigar y discutir
- ¿Cómo clasificaría (IaaS, PaaS o SaaS) el servicio de hosting wordpress ofrecido por wordpress.com y porqué?
- ¿Qué elementos son necesarios para tener un Data Center _On Premise_?
- ¿Qué puede justificar tener servidores propios en un _Housing_ en Chile?
- ¿Qué son los _TIER_ de los Data Center? ¿Cuál es el mayor TIER que posee un Data Center en Chile? ¿Cuál cree es la mayor dificultad en Chile para un TIER 4?
- ¿Cuáles considera que pueden ser las ventajas y desventajas de migrar un Data Center On Premise a la nube?

## Para hacer
- Investigue cómo instalar Docker en su sistema operativo e instalelo ([Instrucciones acá][dockerinstall]). Revise su instalación corriendo el comando ```docker run hello-world```


## Referencias:
1: La nube no es más que un conjunto de centros de datos o "Data Center" que prestan su servicio a terceros, por ejemplo: AWS, Azure, GCP, IBM Cloud, Oracle Cloud, Huawei, entre otros proveedores.


[dockerinstall]: https://docs.docker.com/engine/install/
