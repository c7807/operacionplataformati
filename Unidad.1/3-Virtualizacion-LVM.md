# Resolución del ejercicio
1. Cree la maquina virtual con disco LVM
1. Agregue un nuevo disco
1. Ingrese como "**root**" y siga los siguientes comandos:
   1. Revise las particiones LVM con
      ```bash
      lvs
      ```
   1. Revise los grupos de volumen LVM con
      ```bash
      vgs
      ```
   1. Revise los volumenes físicos LVM con
      ```bash
      pvs
      ```
   1. Revise el espacio del disco con
      ```bash
      df -h
      ```
   1. Revise los dispositivos de bloque
      ```bash
      lsblk -f
      ```
   1. Cree una partición LVM en el nuevo disco con:
      ```
      fdisk /dev/sdb
       p
       n
       p
       1
       [enter]
       [enter]
       p
       t
       8e
       p
       w
      ```
   1. Revise el nuevo bloque(partición) creado
      ```bash
      lsblk -f
      ```
   1. Cree el Volumen físico en la nueva partición
      ```bash
      pvcreate /dev/sdb1
      ```
   1. Revise los volumenes físicos LVM con ```pvs``` y compare con el resultado anterior
   1. Extienda el volumen group
      ```bash
      vgextend debian-vg /dev/sdb1
      ```
   1. Revise el volumen group LVM con ```vgs``` y compare con el resultado anterior
   1. Extienda la partición / (root)
      ```bash
      lvextend -l +100%FREE /dev/debian-vg/root
      ```
   1. Extienda el "file system" o sistema de archivos
      ```bash
      resize2fs /dev/debian-vg/root
      ```
   1. Revise el espacio del disco con ```df -h``` y compare con el resultado anterior.
