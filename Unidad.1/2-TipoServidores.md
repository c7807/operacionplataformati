# Tipo de Servidores

## Introducción
Los servidores son grandes dispositivos de procesamiento y almacenamiento de datos que existen como hardware o como almacenes virtuales ubicados en Internet. Las computadoras o los sistemas de software actúan como servidores que se conectan a una red.

Un servidor puede ser cualquier tipo de dispositivo que comparta y guarde información. Los servidores pueden almacenar y procesar información dentro de su propio sistema o solicitarla a otro.

Los servidores comenzaron como pequeños dispositivos que simplemente transferían datos a una computadora más funcional y luego crecieron en tamaño y capacidad para realizar funciones más complejas.

Ahora, en cuanto a los tipos, existe más de una forma de clasificar a los servidores, puede ser por sus características o en cuanto a los servicios que despliega. En cuanto a sus características, estos pueden ser físicos, virtuales o en contenedor. En cuanto a los servicios son muy variados y por por nombrar algunos sin que la lista sea excluyente, pueden ser:  web, de base de datos, de correo, de DNS, de proxy reverso, de impresión, de aplicación, de archivos, de monitoreo, etc.

## En características
### Físico
Un servidor físico es la pieza de hardware que posee una tarjeta madre, uno o más procesadores, memoria RAM, tarjetas de red, uno o más discos duros y una fuente de poder.

### Virtual
Un servidor virtual comparte procesador y memoria con el equipo físico, es posible reservar cores y parte de la memoria de forma específica, además se puede entregar componentes de red y discos virtuales o físicos. Sin embargo, se emula una maquina por completo, el sistema operativo corre de forma independiente del sistema del servidor físico base, es decir, se encuentra el sistema operativo base del servidor físico que se comunica con el HAL y este a su vez con los controladores de hardware, y sobre el sistema operativo corre un emulador de hardware con sus propios controladores, HAL y sistema operativo sobre la maquina virtual. Esto provoca que un servidor virtual sea más lento que un servidor físico. Ejemplo, si escribo sobre el servidor virtual, el programa llama al sistema operativo virtualizado, este al HAL del disco, ese al controlador del disco virtual y ese le envía la señal de escritura al software de virtualización, que envía el comando al sistema operativo base, este al HAL de disco, al controlador de disco físico y recién se realiza la escritura en el disco físico. ![Aruitectura de Maquina Virtual][vm]

### Contenedor
Un servidor en contenedor, comparte los recursos físicos, es posible dedicar procesamiento y memoria, en este sentido similar al virtualizado, sin embargo, la gran diferencia es que comparte recursos del sistema operativo, por lo que una escritura a disco, por ejemplo, es similar a lo siguiente: el programa invoca al runtime, el runtime al docker engine, docker engine llama al sistema operativo, éste al HAL del disco, ese al controlador y se realiza la escritura en el disco físico. ![Arquitectura Docker][docker]

## En Servicios Desplegados
### Servidor Web
Este es uno de los tipos de servidores más utilizados. Trabaja con los protocolos HTTP (Hypertext Transfer Protocol [RFC7230][rfc7230]) y HTTPS (HyperText Transfer Protocol Secure). Este protocolo se usa para enviar el contenido de un sitio web desde el servidor al navegador. Los servidores web más utilizados son Nginx, Apache y LiteSpeed (aprox. con el el 75% de los servidores web a nivel mundial<sup>[1][a]</sup>), estos tres son Software Libre y los dos primeros son compatibles con variados sistemas operativos (Linux, Unix, Windows). De forma privativa, Microsoft posee su propio servidor web llamado IIS (con el 6% del mercado) que corre en Windows Server.

### Servidores de Correo
Un servidor de correo almacena y entrega el correo a los clientes a través de plataformas de servicios de correo electrónico. Debido a que los servidores de correo están configurados para conectarse continuamente a una red, los usuarios individuales pueden acceder a su correo electrónico sin ejecutar ningún sistema a través de los clientes en sus propios dispositivos.

Los servidores de correo trabajan con los protocolos SMTP, IMAP y POP3. SMTP (Simple Mail Transfer Protocol [RFC821][rfc821]) o Protocolo Simple de Transferencia de Correo. IMAP (Internet Message Access Protocol [RFC3501][rfc3501]) o Protocolo de Acceso a Mensajes de Internet. POP3 es la tercera versión que se ha desarrollado del estándar POP (Post Office Protocol [RFC1939][rfc1939]) o Protocolo de Oficina Postal. SMTP se usa para enviar correos electrónicos desde el programa de correo desde computador del usuario hacia el servidor, POP es para transferir los mensajes desde el servidor hacia el programa de correo, una vez realizada la "descarga" se elimina del servidor. IMAP se usa para transferir los mensajes desde el servidor hacia el programa de correo sin realizar la eliminación del mensaje. Hoy en día dado el SaaS, la mayor parte de los correos pertenecen a la Suite de Google con un 20% de los correos a nivel mundial<sup>[2][b]</sup>.

### Servidores de Base de Datos
Los servidores de Base de datos contienen un motor de base de datos que pueden ser consultados a través de [TCP/IP][rfc1180], por lo general tienen gran capacidad de almacenamiento. Los motores pueden ser relacionales o no relacioanles. Los motores relacionales más populares son: PostgreSQL, MySQL, Oracle y SQL Server. Dentro de los motores NoSQL más conocidos están MongoDB, Redis y Dynamo. Cada motor de base de datos tiene un cliente o controlador propio para la conexión con el motor. Algunas herramientas como DBeaver es capaz de conectarse a múltiples motores de Bases de Datos.

### Servidor de Nombres de Dominio o DNS
Este servidor, por su funcionalidad, es probable sea el más utilizado, sin embargo, es transparente para el usuario final, dado a que presta servicio a los navegadores, servidores de correo, entre otros.

Estos servidores transforman nombres de dominio legibles en direcciones IP. El servidor DNS toma los datos de búsqueda de un usuario y encuentra la dirección solicitada para enviarla al dispositivo del cliente. Funciona basado en el protocolo DNS ([RFC1034][rfc1034]).

Siempre que consultamos un servidor por su nombre, el servidor DNS resuelve la IP, y a través de la IP es que llegamos al servidor, ya sea en el navegador, correo electrónico u otro servicio, dado a que la comunicación entre máquinas es a través de la IP (en Internet que funciona bajo [TCP/IP][rfc1180]).

### Servidor de impresión
Tal como indica su nombre, a este servidor llegan todos los documentos a imprimir, normalmente usan el protocolo IPP (Internet Prineter Protocol [RFC8011][rfc8011]) y sirve como base para la gestión de impresoras, solicitudes de impresión y colas de impresión. Por el servicio que presta, normalmente estos servidores se encuentran dentro de la LAN, aunque cada vez son menos comunes dado a que las impresoras empresariales traen internamente este servicio, aunque aún se encuentran algunos servidores CUPS en algunas empresas.

## Para pensar
- ¿Cuál cree es la importancia que los protocolos de internet se encuentren normados y sean públicos?
- Basado en los protocolos de correo [SMTP][rfc821] ¿cree sea factible crear una librería en PHP (u otro lenguaje) para el envío directo de correo desde su servidor web al servidor de correo?
- Inicialmente solamente habían servidores físicos, luego aparecieron los virtuales para aprovechar mejor el hardware, evolucionó a los contenedores que lo aprovechan aún más. ¿Cómo imagina el siguiente paso en cuanto al aprovechamiento de recursos y performance?

## Para investigar y discutir
- ¿Qué significa RFC?
- ¿Qué es el IETF?
- ¿Qué cree que sucedería con Internet si la IETF dejara de existir?
- ¿Qué es el Software Libre y cómo ha influenciado en que Internet sea lo que es hoy?

## Para hacer
Despliegue en su máquina un Docker de Postgres, conectese a él y cree una nueva BD con una Tabla (este ejercicio supone que ya tiene docker instalado en su maquina).

```bash
# Despliegue el servicio de Postgres
docker run --name postgres -e POSTGRES_PASSWORD=secret -d postgres
# Revise los contenedores ejecutandose
docker ps
# Conectese al contenedor postgres
docker exec -it postgres bash
# Abra el cliente de PostgreSQL con el usuario postgres
psql -U postgres
```

Ejecute los siguientes comandos PostgreSQL:

```SQL
\l
CREATE DATABASE test;
\c test;
\l
CREATE TABLE persona (
  nombre varchar(40) NOT NULL,
  id integer PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY
);
INSERT INTO persona (nombre) VALUES ('Juan'),('Maria'),('Ana');
INSERT INTO persona (nombre) VALUES ('Pedro');
SELECT * FROM persona;
\q
```

Debería mostrar algo similar a:

```
postgres-# \l
                                 List of databases
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
-----------+----------+----------+------------+------------+-----------------------
 postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
 template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
(3 rows)
postgres-# CREATE DATABASE test;
CREATE DATABASE
postgres=# \l
                                 List of databases
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
-----------+----------+----------+------------+------------+-----------------------
 postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
 template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 test      | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
(4 rows)
postgres=# \c test;
You are now connected to database "test" as user "postgres".
test=# CREATE TABLE persona (
  nombre varchar(40) NOT NULL,
  id integer PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY
);
CREATE TABLE
test=# INSERT INTO persona (nombre) VALUES ('Juan'),('Maria'),('Ana');
INSERT 0 3
test=# INSERT INTO persona (nombre) VALUES ('Pedro');
INSERT 0 1
test=# SELECT * FROM persona;
 nombre | id
--------+----
 Juan   |  1
 Maria  |  2
 Ana    |  3
 Pedro  |  4
(4 rows)
test=# \q
root@1a2feae62189:/# exit
```

## Referencias:
1. [w3tech - web server - Febrero 2022][a]
2. [w3tech - email server - Febrero 2022][b]

[a]: https://w3techs.com/technologies/overview/web_server
[b]: https://w3techs.com/technologies/overview/email_server
[docker]: ../img/docker.png
[vm]: ../img/vm.png

[rfc821]: https://datatracker.ietf.org/doc/html/rfc821
[rfc1034]: https://datatracker.ietf.org/doc/html/rfc1034
[rfc1180]: https://datatracker.ietf.org/doc/html/rfc1180
[rfc1939]: https://datatracker.ietf.org/doc/html/rfc1939
[rfc3501]: https://datatracker.ietf.org/doc/html/rfc3501
[rfc7230]: https://datatracker.ietf.org/doc/html/rfc7230
[rfc8011]: https://datatracker.ietf.org/doc/html/rfc8011
