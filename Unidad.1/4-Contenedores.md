# Contenedores
## Introducción
El interés por aprovechar mejor los recursos llevo a las maquinas virtuales, el siguiente paso en la evolución de la tecnología fue el contenedor. Los contenedores es una tecnología muy usada hoy en día, existen diversas formas de administrarlos, comenzaremos de forma manual, para llegar a las automatizadas con ansible y kubernetes.

## Cómo funciona la tecnología de contenedores
![Arquitectura Docker][docker]

Como se explicó en los de tipos de servidores, los servidores en contenedor docker, se ejecutan invocando directamente desde el docker engine características del sistema operativo, por lo que se ahorra la capa de un nuevo sistema operativo virtual.

## Usando contenedores
El repositorio de imágenes de contenedor más conocido es "Docker Hub Container Image Library", alias [Docker Hub][dockerhub]. En él se encuentran las imágenes base de múltiples software, framework y servidores.

### Instalación
Depende la instalación del sistema operativo base, se requiere un sistema operativo reciente (dado a que la tecnología de Docker nace en 2014), por ser: debian 10 u 11, CentOS 7 u 8, Windows 10 u 11, Ubuntu 18.04 LTS o superior, entre otros.

Se recomienda visitar la página oficial y seguir los [pasos de instalación][dockerinstall]

### Comandos básicos de docker
La siguiente no es una lista completa, sino más bien, una breve guía de los comandos más utilizados en docker. Se recomienda fuertemente leer las referencias de [docker cli][dockerref]
1. ```docker ps [-a]```

   Lista los contenedores, con la opción -a lista tambien los que no se encuentran en ejecución.
1. ```docker inspect <nombre-contenedor|id-contenedor>```

   Regresa información del objeto docker según su nombre o id

1. ```docker image ls```

   Lista las imagenes disponibles.

1. ```docker network ls```

   Lista las redes disponibles.

1. ```docker exec [-i -t] <nombre-contenedor|id-contenedor> <comando>```

   Ejecuta un "_comando_" en un contenedor (identificado pro id o por nombre). La opción -i es que debe ser interactivo, la opción -t es que debe abrir un terminal tty.

1. ```docker run [--name <string>] [-d] [-e env-vars] [-v path-local:path-contenedor] <image>```

   Ejecuta un nuevo contenedor de la imagen "image".La opción --name es para definir un nombre, la opción -d es para que corra en segundo plano, la opción -e es para definir una lista de variables de entorno, ésta última también puede ser --env-file seguida por el path de un archivo con las variables de entorno a definir. La opción -v sirve para montar volumenes locales en rutas del contenedor.

1. ```docker start <nombre-contenedor|id-contenedor>```

   Iniciar la ejecución de un contenedor detenido.

1. ```docker stop <nombre-contenedor|id-contenedor>```

   Detener la ejecución de un contenedor.

1. ```docker logs <nombre-contenedor|id-contenedor>```

   Revisar los log (registros) del contenedor.

## Crear una imagen de contenedor basado en una existente
Ya vimos que con _docker run_ uno puede crear un contenedor y a éste se le puede entregar un parámetro de ejecución. Por ejemplo:
```bash
docker run --rm --name holarun -d -v "$PWD":/var/www/html php:7.4-apache
```
con esto ejecuta un contenedor php con el script index.php, el cual podría ser el siguiente:
```php
<?php
    echo date('h:i:s') . " Hola Mundo\n";
```

Para efectos de test de un script puede resultar práctico, pero es mejor definir una imagen propia donde se cree una imagen con los parámetros ya incorporados.

Ahora, si se torna más complejo, quizás ya no baste solamente con montar un directorio con el código, sino que además puede que se necesiten instalar otras dependencias. En estos casos se debe construir una imagen propia para que satisfaga todas las necesidades.

### Construyendo un Dockerfile
Un Dockerfile es el archivo que contiene la receta para crear una nueva imagen. En esta receta como  mínimo se debe expresar la fuente de origen o base "FROM" y la copia del archivo o directorios del código, o bien un comando a ejecutar.

Para nuestro caso, la receta podría ser:
```bash
FROM php:7.2-apache
# Definir el directorio de trabajo
WORKDIR /var/www/html
# Copiar el directorio actual como el directorio de apache
COPY . /var/www/html
```

Luego de tener el archivo Dockerfile los pasos son los siguientes:

1. **docker build**, construye una imagen docker

   Ejemplo:
   ```bash
   docker build . -t hola:latest
   ```

1. Para probar la imagen creamos un docker desde la imagen que creamos
   ```bash
   docker run --name holadocker -d --rm hola:latest
   ```

Una referencia completa la puede encontrar en la [documentación oficial][dockerfile]

## Para pensar
1. ¿Qué tendría que hacer para que el script php me de la hora local de Chile?

## Para investigar
1. ¿Qué debo incluir en el Dockerfile si deseo modificar el archivo php.ini y cambiar el limite de memoria a usar por php a 64 MB?
1. ¿Que es el CI/CD y qué rol cumple el archivo Dockerfile en él?


## Para hacer
- Ingrese a Dockerhub y revise que imagenes oficiales existen, pruebe alguna de su interes en su equipo y practique los comandos run, start, stop, ps, etc.

[docker]: ../img/docker.png
[dockerhub]: https://hub.docker.com/
[dockerinstall]: https://docs.docker.com/engine/install/
[dockerref]: https://docs.docker.com/engine/reference/commandline/docker/
[dockerfile]: https://docs.docker.com/engine/reference/builder/
