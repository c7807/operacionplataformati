# El archivo Dokerfile

## Introducción
El archivo Dockerfile nos es útil para construir nuestra propia imagen de contenedor. Gracias a ello podemos empaquetar en una imagen el sistema que desarrollemos o hacer una personalización de alguno existente.

## Componentes
Los componentes de un Dockerfile pueden ser:

- FROM
- RUN
- VOLUME
- ENV
- LABEL
- ADD
- EXPOSE
- WORKDIR
- COPY
- CMD
- SHELL
- ENTRYPOINT
- ARG

entre otros, para la lista completa visite https://docs.docker.com/engine/reference/builder/

## Comenzando...
La clase pasada se explicó brevemente FROM, WORKDIR y COPY. Hoy probaremos ARG, ENV y RUN

- **RUN** permite la ejecución de un comando en la imagen que se está creando.
- **ENV** permite crear una variable de entorno, se puede inicializar en un valor predeterminado
- **ARG** permite crear una variable de tiempo de construcción, se puede inicializar en un valor predeterminado

## Ejemplos
- Tener una imagen de php-apache actualizada al día de hoy

  Dockerfile
  ```bash
  FROM php:7.4-apache
  RUN apt update && apt upgrade -y
  ```

  Construcción:
  ```bash
  docker build -t my-php-apache-update .
  ```

- Tener la imagen de php-apache con hora local de Santiago

  Dockerfile
  ```bash
  FROM php:7.4-apache
  RUN ln -sf /usr/share/zoneinfo/America/Santiago /etc/localtime
  RUN echo "America/Santiago" > /etc/timezone
  ```

  Construcción:
  ```bash
  docker build -t my-php-apache-localtime .
  ```

  Solución usando ARG en el Dokerfile
  ```bash
  FROM php:7.4-apache
  ARG zona=America/Santiago
  RUN ln -s /usr/share/zoneinfo/$zona /etc/localtime
  RUN echo "$zona" > /etc/timezone
  ```

  Construcción:
  ```bash
  docker build -t my-php-apache-localtime .
  ```

  o también entregando una zona en el momento de la construcción:
  ```bash
  docker build -t server-los-angeles --build-arg zona=America/Los_Angeles .
  ```

- Tener una maquina que regrese el nombre de creación

  index.php
  ```php
  <?php
  echo "Hola desde '. $getenv('name');
  ```

  Dockerfile
  ```bash
  FROM php:7.4-apache
  ENV name=servidor_NN
  COPY index.php /var/www/html
  ```

  Construcción:
  ```bash
  docker build -t hola-server .
  ```

  Ejecución:
  ```bash
  docker run --name server1 -e name=server1 hola-server
  ```

## Ejercicios
- Tener una imagen de php-apache actualizada al día de hoy con hora local de Santiago
- Tener una imagen de php-apache actualizada al día de hoy con hora local de Santiago con una pagina index.php que entregue la hora oficial en Chile Continental.
- Modificar la imagen docker anterior para pasar por argumento la hora local.

## Desafío
- Cree una pagina que se conecte a una base de datos, monte la pagina en un servidor php-apache y suba su contenedor tal que se conecte a otro servidor de BD, mantenga la base de datos en un contenedor docker de una imagen MySQL.
