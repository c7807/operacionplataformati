# Kubernetes
Kubernetes (tambien conocido como k8s) es una plataforma open source para la organización en contenedores que automatiza muchos de los procesos manuales involucrados en la implementación, la gestión y el ajuste de las aplicaciones que se alojan en ellos. Originalmente diseñado por Google y donado a la Cloud Native Computing Foundation. Soporta diferentes entornos para la ejecución de contenedores, incluido Docker.​​

Kubernetes facilita la automatización y la configuración declarativa. Tiene un ecosistema grande y en rápido crecimiento. El soporte, las herramientas y los servicios para Kubernetes están ampliamente disponibles.

Kubernetes no es una Plataforma como Servicio (PaaS) convencional. Ya que Kubernetes opera a nivel del contenedor y no a nivel del hardware, ofrece algunas características que las PaaS también ofrecen, como deployments, escalado, balanceo de carga, registros y monitoreo.

## Instalación
Kubernetes se puede ejecutar en diversas plataformas, en AWS existe EKS (Elastic Kubernetes Service), en Azure se llama AKS (Azure Kubernetes Service) y en GCP se llama GKE (Google Kubernetes Engine).

Para efectos de entrenamiento y pruebas en el computador local, usaremos kubernetes con minikube (en el equipo local). La instalación predeterminada asume que estamos utilizando docker. Además de docker debemos instalar minikube propiamente tal y kubectl (que es una interfaz de despliegue de comandos a k8s).

Recomendación, instale docker, luego kubectl y después minikube.

### instalar kubectl en linux
Los pasos en linux básicamente son:

```bash
STABLE=$(curl -L -s https://dl.k8s.io/release/stable.txt)
curl -LO "https://dl.k8s.io/release/${STABLE}/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

Para verificar puede revisar la versión

```bash
kubectl version --short --client
```


### instalar minikube en linux
Las instrucciones detalladas se encuentran [_**aquí**_][minikube], pero básicamente en una distribución linux los pasos son:

```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
minikube start
```


## Breve explicación de Componentes y recursos en k8s
A continuación se realizará una lista no exaustiva de recursos de k8s, solamente se listarán los más comunes.

### nodes
Un nodo es una máquina de trabajo en Kubernetes, previamente conocida como "minion". Un nodo puede ser una máquina virtual o física, dependiendo del tipo de clúster. la sumatoria de recursos (CPU y RAM) disponibles, se obtiene de los nodos del clúster.

Para ver o listar los nodos:
```bash
kubectl get nodes
```

### namespaces
Los namespaces o espacios de nombre son múltiples clústeres virtuales alojados en el mismo clúster físico. 

Para ver o listar los espacios de nombre ejecute:
```bash
kubectl get namespaces
```

### configmaps
Un configmap es un objeto de la API utilizado para almacenar datos no confidenciales en el formato clave-valor. Los Pods pueden utilizar los ConfigMaps como variables de entorno, argumentos de la linea de comandos o como ficheros de configuración en un Volumen.

Un ConfigMap te permite desacoplar la configuración de un entorno específico de una imagen de contenedor, así las aplicaciones son fácilmente portables.

Para ver o listar los espacios de nombre ejecute:
```bash
kubectl -n espacio get configmaps
```

### deployment
Un controlador de Deployment proporciona actualizaciones declarativas para los Pods y los ReplicaSets.

Para listar lis deployment de un espacio de nombre puede ejecutar:
```bash
kubectl -n espacio get deployments
```

### replicaset
El objeto de un ReplicaSet es el de mantener un conjunto estable de réplicas de Pods ejecutándose en todo momento. Así, se usa en numerosas ocasiones para garantizar la disponibilidad de un número específico de Pods idénticos.

Para listar los replicaset de un espacio de nombre puede ejecutar:
```bash
kubectl -n default get replicasets
```

### pod
Los Pods son las unidades de computación desplegables más pequeñas que se pueden crear y gestionar en Kubernetes. Los contenidos de un Pod son siempre coubicados, coprogramados y ejecutados en un contexto compartido. Un Pod modela un "host lógico" específico de la aplicación: contiene uno o más contenedores de aplicaciones relativamente entrelazados.

Para listar los pod de un espacio de nombre puede ejecutar:
```bash
kubectl -n espacio get pod
```

### hpa
Un hpa (Horizontal Pod Autoscaler) actualiza automáticamente la cantidad de un recurso según la carga de trabajo, con el objetivo que el recurso coincida con la demanda.

El escalado horizontal significa que la respuesta al aumento de la carga es implementar más pods. (Esto es diferente al escalado vertical , que para Kubernetes significaría asignar más recursos a los pods que ya se están ejecutando para la carga de trabajo).

Si la carga disminuye y la cantidad de pods está por encima del mínimo configurado, hpa le indica al recurso de carga de trabajo que reduzca la escala.

Para listar el estado del hpa de un espacio de nombre puede ejecutar:
```bash
kubectl -n espacio get hpa
```

### services
Los servicios es el objeto de la API de Kubernetes que describe cómo se accede a las aplicaciones, tal como un conjunto de Pods, y que puede describir puertos y balanceadores de carga.

Con Kubernetes no necesitas modificar tu aplicación para que utilice un mecanismo de descubrimiento de servicios desconocido. Kubernetes le otorga a sus Pods su propia dirección IP y un nombre DNS para un conjunto de Pods, y puede balancear la carga entre ellos.

Los ServiceTypes de Kubernetes permiten especificar qué tipo de Service quieres. El valor por defecto es ClusterIP

Los valores Type y sus comportamientos son:

- ClusterIP: Expone el Service en una dirección IP interna del clúster. Al escoger este valor el Service solo es alcanzable desde el clúster. Este es el ServiceType por defecto.

- NodePort: Expone el Service en cada IP del nodo en un puerto estático (el NodePort). Automáticamente se crea un Service ClusterIP, al cual enruta el NodePortdel Service. Podrás alcanzar el Service NodePort desde fuera del clúster, haciendo una petición a NodeIP:NodePort.

- LoadBalancer: Expone el Service externamente usando el balanceador de carga del proveedor de la nube. Son creados automáticamente Services NodePorty ClusterIP, a los cuales el apuntará el balanceador externo.

- ExternalName: Mapea el Service al contenido del campo externalName (ej. foo.bar.example.com), al devolver un registro CNAME con su valor. No se configura ningún tipo de proxy.

>Nota:
>
>Necesitas la versión 1.7 de kube-dns o CoreDNS versión 0.0.8 o más para usar el tipo ExternalName.

También puedes usar un Ingress para exponer tu Service. Ingress no es un tipo de Service, pero actúa como el punto de entrada de tu clúster. Te permite consolidar tus reglas de enrutamiento en un único recurso, ya que puede exponer múltiples servicios bajo la misma dirección IP.

Se recomienda para mayor profundidad revisar la [_**documentación de services**_][service]


Para ver o listar los servicios de un espacio de nombre ejecute:
```bash
kubectl -n espacio get services
```

### ingress
Ingress expone las rutas HTTP y HTTPS desde fuera del clúster a los servicios dentro del clúster. El enrutamiento del tráfico está controlado por reglas definidas en el recurso ingress. 

En el ingress se puede configurar el balanceador de carga hacia un servicio, exponer uno o varios servicios internos, proveer del certificado SSL/TLS, también puede configurar su enrutador perimetral o interfaces adicionales para ayudar a manejar el tráfico.

Para verel ingress de un espacio de nombre ejecute:
```bash
kubectl -n espacio get ingress
```

## kubectl
Kubernetes proporciona una herramienta en línea de comandos para comunicarse con el clúster de Kubernetes mediante la API de comandos de Kubernetes, esta herramienta es kubectl.

### Sintaxis:
```bash
kubectl [command] [TYPE] [NAME] [flags]
```
1. command: 
   Los comandos más comunes son:
    - get : lista un recurso
    - describe : describe un recurso
    - edit : edita un recurso
    - apply : aplica un archivo yaml con la definición de un recurso
    - delete : elimina un recurso
    - logs : revisa el log de un pod
    - exec : ejecuta un comando en un pod

1. TYPE: son los tipos de recursos, como los vistos en la sección anterior.
1. NAME: es el nombre del recurso, por ejemplo el nombre del pod
1. flags: son los complementos, por ejemplo, el flag **-n** indica el namespace al cual se hace referencia

Ejemplo:
```bash
kubectl logs auth-b8689b995-5h4lq -n api
```

- el comando es **logs**
- el nombre del pod es **auth-b8689b995**
- el flag es **-n api**

Para más detalles visite la [_**documentación**_][kubectl]

## Archivos YAML de creación
Los siguientes son solamente ejemplos, no incluyen todos los componentes que existen, pero sirven de base. Se recomienda fuertemente revisar la documentación de kubernetes para mayor detalle.

Los archivos yaml tienen una versión de la api (apiVersion), la metadata (donde se encuentra el nombre del recurso y de forma optativa labels y el nombre del namespace). Después dependiendo del archivo puede venir etiquetas data o spec.

A forma de ejemplo los siguientes archivos suponen el despliegue de un aplicativo cuya imagen esta almacenada en **git.example.com:4567/api/php-api-backend:master** y se desplegará bajo la url: **http://api-backend.example.com**, inicialmente con 3 pod con la posibilidad de crecer elasticamente hasta 60 pod.

La aplicación de los archivos yaml se realiza con el comando:
```bash
kubectl apply -f archivo.yaml
```

### namespace
1-namespace.yml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: api-backend
```

### configmaps
2-configmaps.yaml
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: config-file
  namespace: api-backend
  labels:
    tier: backend
data:
  url_api: http://api-backend.example.com
  database: |
    DB_HOST=db.example.com
    DB_PASS=dfghj678&1PK
    DB_USER=policarpo
    DB_NAME=api_backend
```

### deployment
3-deployment.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: php-api-backend
  namespace: api-backend
  labels:
    tier: backend
spec:
  # replicaset 3 pods
  replicas: 3
  selector:
    matchLabels:
      app: php-api-backend
      tier: backend
  template:
    metadata:
      labels:
        app: php-api-backend
        tier: backend
    volumes:
    - name: config
        configMap:
          name: config-file
          items:
          - key: database
            path: database.ini
    spec:
      containers:
      - name: php
        image: git.example.com:4567/api/php-api-backend:master
        imagePullPolicy: Always
        ports:
        - name: php-port
          containerPort: 8080
        livenessProbe:
          tcpSocket:
            port: php-port
          initialDelaySeconds: 40
          periodSeconds: 5
        readinessProbe:
          tcpSocket:
            port: php-port
          initialDelaySeconds: 40
          periodSeconds: 5
        resources:
          limits:
            cpu: "200m"
            memory: "256Mi"
          requests:
            cpu: "100m"
            memory: "64Mi"
        env:
          - name: APP_URL
            valueFrom:
                configMapKeyRef:
                    name: config-file
                    key: url_api
        volumeMounts:
        - name: config
          mountPath: /usr/src/app/config/
```

### hpa
4-hpa.yaml
```yaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: api-backend-scaler
  namespace: api-backend
spec:
  scaleTargetRef:
    kind: ReplicaSet
    name: php-api-backend
  minReplicas: 3
  maxReplicas: 60
  targetCPUUtilizationPercentage: 40
```

### services
5-services.yaml
```yaml
apiVersion: v1
kind: Service
metadata:
  name: php-service-api-backend
  namespace: api-backend
  labels:
    tier: backend
spec:
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
  type: NodePort
  selector:
    app: php-api-backend
    tier: backend
```

### ingress
6-ingress.yaml
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: api-backend-ingress
  namespace: api-backend
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  rules:
  - host: api-backend.example.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: php-service-api-backend
            port:
              number: 80
```

## Ejercicios propuestos:
- De las imáganes creadas para la evaluación 2, seleccione una y cree el despliegue del aplicativo.
- Cree 2 servicios, tal que uno dependa del otro, por ejemplo, un php-fpm que requiere ser mostrado a través de un servicio nginx.
- Cree 2 servicios, tal que uno complemete al otro, por ejemplo, un backend python que es mostrado a través de un frontend react.

## Bibliografía
Conceptos de k8s: [_**https://kubernetes.io/es/docs/concepts/overview/**_][kube]

[kube]: https://kubernetes.io/es/docs/concepts/overview/
[service]: https://kubernetes.io/es/docs/concepts/services-networking/service/
[minikube]: https://minikube.sigs.k8s.io/docs/start/
[kubectl]: https://kubernetes.io/docs/reference/kubectl/