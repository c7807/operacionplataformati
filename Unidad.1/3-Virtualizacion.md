# Virtualización
## Introducción
Un servidor virtual, emula una máquina real, el sistema operativo montado en ella no sabe que su maquina no es real, accede a los recursos que le han sido entregados y solo a ellos conoce. Un término que encontramos frecuentemente en las empresas que venden este tipo de "máquina" es VPS (Virtual Private Server). Las características de un VPS es siempre inferior al de los componentes físicos donde se encuentra alojado, no es posible compartir recursos que no se tienen, además, la maquina base tiene su propio sistema operativo y también consume recursos con el software de virtualización.

Es destacable, que una máquina física, dependiendo de sus características, puede alojar múltiples maquinas virtuales, eso ayuda a que sus recursos sean mejor aprovechados, pues cada máquina contiene su propio software, con lo que se pueden, por ejemplo, tener varias versiones de un sistema corriendo en paralelo sin que las librerías topen entre si.
![diagrama de arquitectura][vm]

## Sistemas de virtualización
Existen variados software de virtualización, dejaré unas pequeñas referencias a algunos de los más populares, las primeras 3 son las más utilizadas en soluciones empresariales, incluso la cuarta de una forma indirecta, dado a que hay muchas herramientas basadas en KVM (incluso AWS que [migró de Xen a KVM][xentokvm] y RHV). Dependiendo del tamaño de la empresa y decisiones empresariales que se tengan, puede ser que se utilice un virtualizador de forma directa o de forma indirecta a través del uso de AWS, Azure, GCP, etc.

### VMWare
VMware Inc., es una filial de EMC Corporation (propiedad de Dell) que proporciona software de virtualización disponible para X86. Entre este software se incluyen VMware Workstation, y los gratuitos VMware Server y VMware Player. VMware puede funcionar en Windows, Linux, y macOS que corre en procesadores Intel.

### RHV - RedHat Virtualization
RHEV es una plataforma de virtualización empresarial de RedHat. Se basa en Red Hat Enterprise Linux y KVM (Kernel-based Virtual Machine), utiliza el protocolo SPICE y VDSM (Administrador de servidor de escritorio virtual) con un servidor de administración centralizado basado en RHEL. Puede adquirir información de usuarios y grupos de un dominio de Active Directory o FreeIPA. Algunas de las tecnologías de Red Hat Virtualization provienen de la adquisición de Qumranet por parte de Red Hat. Otras partes derivan de oVirt.

### Xen
Xen se creó como proyecto de investigación de la Universidad de Cambridge liderado por Ian Pratt y Keir Fraser. Su primera versión pública fue lanzada en 2003. Hoy en día se le conoce como Citrix Hypervisor. Oracle lanzó Oracle VM el 2007 basado en XenEnrterprise. Xen es Software Libre licenciado bajo GPL v2.

### KVM
KVM (Kernel-based Virtual Machine) es una solución de virtualización completa para Linux en hardware x86 que contiene extensiones de virtualización (Intel VT o AMD-V). Consiste en un módulo de kernel cargable, kvm.ko, que proporciona la infraestructura de virtualización central y un módulo específico del procesador, kvm-intel.ko o kvm-amd.ko.

KVM es Open Source. El componente kernel de KVM se incluye en la rama principal de Linux, desde el kernel 2.6.20. El componente de espacio de usuario de KVM se incluye en la línea principal QEMU, a partir de 1.3.

### VirtualBox
Oracle VM VirtualBox (conocido generalmente como VirtualBox) es un software de virtualización para arquitecturas x86 desarrollado inicialmente por Innotek el 2007, adquirido por SUN el 2008 y por Oracle el 2010. Actualmente es desarrollado por Oracle como parte de su familia de productos de virtualización. Posee dos versiones, la versión empresa y la versión OSE (Open Source Edition). La versión OSE no permite conexión RDP ni el soporte iSCSI.

## Para pensar
- ¿Qué problemas cree que vino a resolver la virtualización?
- ¿Cómo cree que el manejo del multihilo del sistema operativo permite trabajar la virtualización?
- Sobre el 60% de los data center pertenecen a AWS, Azure y GCP, ¿cómo puede afectar las leyes de EEUU sobre la provisión de servicios a diversos países?
- Dado a que mediante software se puede virtualizar un recurso para el sistema operativo, cree factible crear una unidad de cómputo, una tarjeta de red o una unidad de almacenamiento a partir de un conjunto de elementos de hardware?

## Para investigar y discutir
- Investigue sobre cómo es el almacenamiento y las unidades de cómputo en los Data Center, y cómo distribuyen los recursos.
- ¿Cómo funciona LVM (Logical Volume Manager) y cómo permite LVM proveer de almacenamiento?, ¿cómo afecta esto al tamaño posible de disco?.
- ¿Cómo funciona el Ethernet Bounding y qué nos permite a nivel de conexiones de red?
- ¿Cómo cree que las tecnologías anteriores influyen en las grándes supercomputadoras?

## Para hacer
- Instale VirtualBox u otro software de virtualización en su equipo en un disco virtual de 4G, sobre él instale Debian 11 con una unidad de disco lvm, luego, agregue un disco de 2G con lvm, deje funcionando como una sola partición de 6G. Para ello siga las instrucciones encontradas en la investigación anterior.


[xentokvm]: https://www.freecodecamp.org/news/aws-just-announced-a-move-from-xen-towards-kvm-so-what-is-kvm/
[vm]: ../img/vm.png
