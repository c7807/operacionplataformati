# El archivo Dockerfile segunda parte
Dentro del presente curso, solo veremos las sentencias más comunes dentro del Dockerfile, no se pretende abarcar cada comando, sino más bien ser una guía de inicio para una profundización personal.

## Revisando otros componentes del Dockerfile
- **ADD y COPY** Agregan archivos a la imagen
- **EXPOSE** Expone un o más puertos de la imagen
- **ENTRYPOINT** Ejecuta un comando
- **CMD** Ejecuta un comando o bien entrega los parametros a ENTRYPOINT


## ¿Cuál es la diferencia entre ADD y COPY?
Ambos agregan archivos a la imagen, sin embargo, ADD tiene un origen de archivo(s) diferente al de COPY, dado a que 1) ADD descomprime el contenido de un archivo comprimido. 2) ADD es capaz de incorporar un archivo desde una URL

### Ejemplo:
```COPY asd.tar.gz /destino``` copiará el archivo asd.tar.gz a la ruta destino.

```ADD asd.tar.gz /destino``` descomprimirá el contenido de asd.tar.gz en destino.

Por lo tanto, su uso podría ser el siguiente:

```bash
FROM python:3
WORKDIR /usr/src/app
# instalar paquetes necesarios
COPY requirements.txt .
RUN pip install -r requirements.txt
# Agregando el codigo
ADD code.tar.gz .
# etc...
```

## EXPOSE
Expone uno o más puertos y su protocolo

### Ejemplo:
```
FROM php:7.4-apache
EXPOSE 80/tcp
EXPOSE 443/tcp
```

## ENTRYPOINT
Ejecuta un comando de la imagen, normalmente el comando del servicio que se ejecutará

Su uso es:
```
ENTRYPOINT ["command", "arg1", "arg2"]
```

también:
```
ENTRYPOINT command arg1 arg2
```

La diferencia entre ambos, es que en el primer caso el proceso 1 es ```command arg1 arg2```, mientras que en el segundo la shell predeterminada, es decir, en el segundo caso se ejecuta ```/bin/sh -c command arg1 arg2```

### Ejemplo:
Supongamos que tenemos un holamnundo.py en FLASK

```python
from flask import Flask
app = Flask(__name__)

@app.route('/')
def index():
    return 'Hola Mundo'

app.run(host='0.0.0.0', port=80)
```

el Dockerfile sería algo así como:

```bash
FROM python:3
WORKDIR /usr/src/app
RUN pip install flask
COPY holamundo.py .
EXPOSE 80
ENTRYPOINT ["python", "holamundo.py"]
```

Si entramos al docker y ejecutamos un top, nos percataremos que el proceso 1 es **python holamundo.py**. Si cambiamos el ENTRYPOINT por: ```ENTRYPOINT python holamundo.py``` el proceso 1 será **/bin/sh -c python holamundo.py** y el proceso 7 será **python holamundo.py**.


## CMD
Ejecuta un comando de la imagen, normalmente el comando del servicio que se ejecutará

Su uso es:
```
CMD ["command", "arg1", "arg2"]
```

también:
```
CMD command arg1 arg2
```

Sin embargo, si lo estas usando con ENTRYPOINT, se usa CMD para entregar parámetros o argumentos al comando
```
ENTRYPOINT ["command"]
CMD ["arg1", "arg2"]
```

### Ejemplo:
Supongamos que tenemos el mismo un holamnundo.py anterior en FLASK, el Dockerfile podría ser algo sí como:
```bash
FROM python:3
WORKDIR /usr/src/app
RUN pip install flask
COPY holamundo.py .
EXPOSE 80
CMD ["python", "holamundo.py"]
```

o bien:
```bash
FROM python:3
WORKDIR /usr/src/app
RUN pip install flask
COPY holamundo.py .
EXPOSE 80
ENTRYPOINT ["python"]
CMD ["holamundo.py"]
```
