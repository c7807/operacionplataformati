# Operación de Plataformas TI
## Unidad 1. Servidor en la nube
1. [Conceptos de plataformas][u1.1]
1. [Tipo de servidores][u1.2]
1. [Virtualización][u1.3]
1. [Docker][u1.4]
   - [Dockerfile primera parte][u1.4a]
   - [Dockerfile segunda parte][u1.4b]
1. Sistema de ticket
1. Monitoreo  de servicios
1. Buenas prácticas en gestión de solicitudes de operación (ITIL)

## Unidad 2. Implementación
1. Levantar un servidor en la nube-cloud
1. Ejecutar una aplicación web


[u1.1]: Unidad.1/1-ConceptosPlataforma.md
[u1.2]: Unidad.1/2-TipoServidores.md
[u1.3]: Unidad.1/3-Virtualizacion.md
[u1.4]: Unidad.1/4-Contenedores.md
[u1.4a]: Unidad.1/4-Contenedores-01.md
[u1.4b]: Unidad.1/4-Contenedores-02.md